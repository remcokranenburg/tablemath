angular.module("tablemath").factory('FormulaParser', function() {
  "use strict";
  
  /**
   * FormulaParser constructor. 
   */

  function FormulaParser() {
    this._functions = {};
    
    // Register some math functions. We want these to become infix eventually.
    
    this.registerFunction("add", function(a, b) {
      return (+a) + (+b);
    });
    
    this.registerFunction("sub", function(a, b) {
      return (+a) - (+b);
    });
    
    this.registerFunction("mul", function(a, b) {
      return (+a) * (+b);
    });
    
    this.registerFunction("div", function(a, b) {
      return (+a) / (+b);
    });
  }
   
  /**
   * Register a function to be executed. Cell formulas can only execute functions that are
   * registered, in order to prevent XSS attacks. I would highly recommend to only register
   * functions that are purely functional: depending only on the input values and returning a value
   * without causing any side effects in the process.
   *
   * @param {string} name Name of the function as used in a cell formula.
   * @param {Function} func Function to be executed
   */

  FormulaParser.prototype.registerFunction = function(name, func) {
    // TODO use a web worker to call the function in a sandboxed environment and return the result
    this._functions[name] = func;
  }

  FormulaParser.prototype.parse = function(formula) {
    return this._parseFormula(formula);
  }

  FormulaParser.prototype._parseFormula = function(formula) {
    // if it can be parsed as number, return number
    // if first character is '=', result = parseExpression(formula.slice(1)), end of string? return result
    // else return string
  }

  FormulaParser.prototype._parseExpression = function(expression) {
    // if it can be parsed as a number, return number
    // if first char '(', result = parseExpression(), search for ')' and return result
    // if identifier plus '(', check function exists, result = functions[fn].call(parseArguments()), find ')', return result
    // if letters then numbers, lookup cell, register as dependant and return value
    // TODO handle infix
  }

  FormulaParser.prototype._parseArguments = function(args) {
    // expressions separated by commas
  }
  
  return FormulaParser;
});

