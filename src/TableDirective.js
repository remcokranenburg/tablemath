
/**
 * Adds spreadsheet functionality to any table that has the "math" attribute defined
 */

angular.module('tablemath').directive('table', function($log, FormulaParser) {
  "use strict";
  
  /**
   * Turns a spreadsheet index string into 0-indexed rows and columns of a table.
   *
   * @param {string} cellIndexString
   * @return {Object} with members c the column index and r the row index
   */

  function cellIndexStringToIndeces(cellIndexString) {
    function charToBase26(c) {
      return c.charCodeAt(0) - "a".charCodeAt(0) + 1; // add one so 'a' -> 1
    }
    
    function base26ArrayToNum(a, b) {
      return a * 26 + b;
    }
    
    var indeces = cellIndexString.match(/([a-z]+)([0-9]+)/i);
    var colStr = indeces[1].toLowerCase();
    
    // map the characters to numbers, then use them as base 26 digits, then subtract 1 for 0-index
    
    var colIndex = Array.map(colStr, charToBase26).reduce(base26ArrayToNum) - 1;
    var rowIndex = indeces[2] - 1;
    
    return { c: colIndex, r: rowIndex };
  }

  /**
   * Extends each HTMLTableElement with a getCell method which selects a cell based on spreadsheet
   * syntax. E.g. "A1" is the top left cell with "A" for column 0 and "1" for row 0.
   
   * @param {string} cellIndexString Index in spreadsheet form (e.g. A1 or AB32)
   * @return HTMLElement, the indexed cell
   */

  HTMLTableElement.prototype.getCell = function(cellIndexString) {
    var indeces = cellIndexStringToIndeces(cellIndexString);
    var row = this.rows[indeces.r];
    
    if(row === undefined) {
      throw new Error("Index out of bounds: row does not exist");
    }
    
    var cell = row.childNodes[indeces.c];
    
    if(cell === undefined) {
      throw new Error("Index out of bounds: col does not exist");
    }
    
    return cell;
  }

  /**
   * Handles blur events on table cells
   *
   * @param {Event|HTMLElement} e
   */

  function blurListener(e) {
    var el = e.target || e; // FIXME this is just ugly
    
    
    if(el.textContent.charAt(0) === '=') {
      // replace formulae with their results and store the formula in el.dataset
      
      el.dataset.formula = el.textContent;
      el.dataset.result = "result of " + el.dataset.formula;
      el.textContent = el.dataset.result;
    }
    else {
      // remove formula from dataset when cell value is not a formula
      
      delete el.dataset.formula;
      el.dataset.result = el.textContent;
    }
  }

  /**
   * Handles focus events on table cells
   *
   * @param {Event} e
   */

  function focusListener(e) {
    var el = e.target;
    if(el.dataset.formula) {
      el.textContent = el.dataset.formula;
    }
  }  
  
  return {
    restrict: 'E',
    link: function(scope, element, attrs) {
      var table = element[0];
      
      if("math" in attrs) {
        $log.log("table with math attribute found; creating spreadsheet");
        
        var cells = table.querySelectorAll("td, th");
        
        Array.forEach(cells, function(el) {
          // make all cells that are not disabled contentEditable
          
          if(!el.hasAttribute('disabled') && !el.hasAttribute('readonly')) {
            el.contentEditable = true;
          }
          
          // register blur and focus listeners
          
          el.addEventListener('blur', blurListener);
          el.addEventListener('focus', focusListener);
          
          // execute blurListener to calculate initial formulae
          
          blurListener(el);
        });
      }
    }
  };
});
