all: bin/tablemath.js bin/tablemath.min.js

clean:
	-rm bin/tablemath.js
	-rm bin/tablemath.min.js

bin/tablemath.min.js: src/*.js
	bower install;
	java \
		-jar bower_components/closure-compiler/lib/vendor/compiler.jar \
		--compilation_level WHITESPACE_ONLY \
		--language_in=ECMASCRIPT5_STRICT \
		--js src/*.js \
		--js_output_file bin/tablemath.min.js

bin/tablemath.js: src/*.js
	cat src/*.js > bin/tablemath.js
